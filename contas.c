#include "contas.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define atrasar() sleep(ATRASO)
		     
int contasSaldos[NUM_CONTAS];
int flag = 0;

int contaExiste(int idConta) {
  return (idConta > 0 && idConta <= NUM_CONTAS);
}

void inicializarContas() {
  int i;
  for (i=0; i<NUM_CONTAS; i++)
    contasSaldos[i] = 0;
}

int debitar(int idConta, int valor) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  if (contasSaldos[idConta - 1] < valor)
    return -1;
  atrasar();
  contasSaldos[idConta - 1] -= valor;
  return 0;
}

int creditar(int idConta, int valor) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  contasSaldos[idConta - 1] += valor;
  return 0;
}

int lerSaldo(int idConta) {
  atrasar();
  if (!contaExiste(idConta))
    return -1;
  return contasSaldos[idConta - 1];
}


void simular(int numAnos) {
    int ano, i, max;
    
    // Imprime as estimativas até ao numAnos
    for (ano=0; ano <= numAnos; ano++) {
        printf("\nSIMULACAO: Ano %d\n",ano);
        printf("=================\n");
        for (i=0; lerSaldo(i+1) != 0 && i < 10; i++) { // Imprime até encontrar conta a zero ou percorrer todas as contas
            printf("Conta %d, Saldo %d\n", i+1, lerSaldo(i+1));
            max = intMax(lerSaldo(i+1) * (1 + TAXAJURO) - CUSTOMANUTENCAO,0);
            if (max != 0) {
                creditar(i+1,lerSaldo(i+1) * TAXAJURO);
                debitar(i+1, CUSTOMANUTENCAO);
            }
        }
        if (i < 10) // Caso nao tenham sido imprimidas todas as contas
            printf("Conta %d, Saldo %d\n", i+1, 0); // Imprime a primeira conta a zero
        if (flag == 1) { // Verifica se a flag foi activada atraves do sinal SIGUSR1
            printf("Simulacao terminada por signal\n");
            exit(3);
        }
    }
}

/*
 intMax:
 Recebe dois inteiros e devolve o maior
 */
int intMax(int a,int b) {
    if (a >= b)
        return a;
    else
        return b;
}
/*
 alteraFlag:
 funcao chamada quando e' recebido o sinal SIGUR1 e ativa a flag
*/
void alteraFlag() {
    flag = 1;
}
